"""
Command Line Interface (CLI) manager for My Module.

TODO: Adds description
"""
import argparse
import logging
import os
import time
import sys
from pathlib import Path

from module import uav_rgb_plantcounting_all
from module import error

PROCESS_NAME = "UAV RGB Plant Counting All"  # TODO: Give the current process a name


def create_parser():
    """Create Command Line Interface arguments parser

     TODO: Define your own CLI arguments. Refer to this tutorial: https://docs.python.org/fr/3/howto/argparse.html
     """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "outputFolder",
        help="path to the directory where the output will be generated")

    parser.add_argument(
        'coco_path',
        help='String representing the path to the coco json resulting from the counting module.'
    )

    parser.add_argument(
        'number_rows',
        type=int,
        help='Integer representing the number of rows in one microplot.'
    )

    parser.add_argument(
        'inter_row',
        type=int,
        help='Integer representing the distance in centimeters between two rows.'
    )

    # Optional arguments
    parser.add_argument(
        '-e', '--emergence_rate',
        action='store_true',
        help='indicate if the emergence rate needs to be calculated.'
    )

    parser.add_argument(
        '-s', '--stocking_density',
        action='store_true',
        help='indicate if the stocking density needs to be calculated.'
    )

    parser.add_argument(
        '-i', '--ignore_side_rows',
        action='store_true',
        help='indicate if side rows should be ignore in the stocking density calculations.'
    )

    parser.add_argument(
        '-d', '--disc_radius',
        type=int,
        help='Integer representing the radius of the discs in centimeters in the stocking density calculations. default : 40'
    )

    parser.add_argument(
        '-r', '--remove_distance',
        type=int,
        help='Integer representing the minimum distance in centimeters between the detected plant and the row to remove the plant. default : 15'
    )

    parser.add_argument(
        "-v",
        "--verbose",
        help="activate output verbosity",
        action="store_true",
        default=False)

    return parser.parse_args()


if __name__ == '__main__':
    timer_start = time.time()
    try:
        # Manage CLI arguments
        args = create_parser()

        # Init Logger
        logger = logging.getLogger()

        if args.verbose:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.addHandler(logging.FileHandler(os.path.splitext(__file__)[0] + '.log', 'w'))

        logging.debug("CLI arguments are valid")

        # Creating output folder if doesn't exist
        if not os.path.exists(args.outputFolder):
            logging.debug("Create output folder '" + args.outputFolder + "'")
            Path(args.outputFolder).mkdir(exist_ok=True, parents=True)

        # Run module core process
        # TODO: Add your own arguments to the run method
        uav_rgb_plantcounting_all.run(
            Path(args.outputFolder),
            Path(args.coco_path),
            args.number_rows,
            args.inter_row,
            args.emergence_rate,
            args.stocking_density,
            args.ignore_side_rows,
            args.disc_radius,
            args.remove_distance
        )

        logging.info("Process '" + PROCESS_NAME + "' complete")

    except error.DataError as e:
        # When DataError is raised, the process fails without killing the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
    except Exception as e:
        # Every other errors kill the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
        sys.exit(1)
    finally:
        timer_end = time.time()
        logging.debug("Process '" + PROCESS_NAME + "' took " + str(int(timer_end - timer_start)) + " seconds")
