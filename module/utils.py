import cv2
import numpy as np


def extract_image(img_id, coco_data):
    """ Read the RGB image of the plot with the id of the image and the data read in the json file produced by
    the counting-av-rgbi-dl module

    :param img_id: int: identification number of the image in the cocojson data
    :param coco_data: dict : data produced by the counting-av-rgbi-dl module
    :return: ndarray: the image corresponding the id
    """
    pth = coco_data.loadImgs(img_id)[0]["path"]
    image_bgr = cv2.imread(pth)
    image_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
    return image_rgb


def extract_feature_from_anns(coco_data, features, img_id, anns_ids=None):
    """ Extract the features wanted from the counting-av-rgbi-dl module predictions stored in "coco_data"

    :param coco_data: COCO: contains the predictions of the counting-av-rgbi-dl module
    :param features: str or list of str: the feature to extract from coco_data (ex: ["bbox", "segmentation"])
    :param img_id: int: identification of the image analysed
    :param anns_ids: list of int (optional) or None: contains the ids of the annotations in which to extract features
    :return: extracted_feature: - one feature: list: contains the features extracted
                                - several features : list of lists: contains the features extracted, in the same
                                                     order of the "features" parameter
    """
    annotations = coco_data.loadAnns(ids=coco_data.getAnnIds(imgIds=img_id))

    if features is not list:
        features = [features]

    extracted_features = []
    if anns_ids is None:
        for feature in features:
            extracted_feature = []
            for annotation in annotations:
                extracted_feature.append(annotation[feature])
            extracted_features.append(extracted_feature)

        if len(extracted_features) == 1:
            return extracted_features[0]
        else:
            return extracted_features

    else:
        for feature in features:
            extracted_feature = []
            for anns_id in anns_ids:
                found = False
                for annotation in annotations:
                    if annotation["id"] == anns_id:
                        extracted_feature.append(annotation[feature])
                        found = True
                if not found:
                    print("no corresponding " + feature + " for id " + str(anns_id))
                    extracted_feature.append(None)
            extracted_features.append(extracted_feature)

        if len(extracted_features) == 1:
            return extracted_features[0]
        else:
            return extracted_features


def check_rows(rows, img_id):
    """ Check if the rows were identified

    :param rows: dict containing the results of the row detection module
    :param img_id: int: indentification number of the image
    :return:    True if the rows were identified, False otherwise
    """
    ids = np.array(rows["rows"]['images']['ids'])
    coefs = rows["rows"]["coefs"]
    img_id_coefs = coefs[np.where(ids == img_id)[0][0]]
    return True if img_id_coefs is not None else False


def extract_coefs_from_img_id(rows, img_id):
    """ Extract the affine function coefficients corresponding to the image identification number

    :param rows: dict containing the results of the row detection module
    :param img_id: int: indentification number of the image
    :return:    coefs_a: float: slope of the rows
                coefs_b: list of floats: b coefficient of the affine function corresponding to each row
    """
    ids = np.array(rows["rows"]['images']['ids'])
    coefs = rows["rows"]["coefs"]
    img_id_coefs = coefs[np.where(ids == img_id)[0][0]]
    coefs_b = img_id_coefs["b"]
    coefs_a = img_id_coefs["a"]
    return coefs_a, coefs_b


def calculate_ratio_cm_pixel(rows, inter_row_cm, img_id):
    """ Calculate the ratio to pass from pixel to cm

    :param rows: dict read from detected_row.json
    :param inter_row_cm: the distance between two rows in the crop
    :param img_id: identification number of the image analysed
    :return: the ratio between one centimeter and one pixel
    """
    _, coefs_b = extract_coefs_from_img_id(rows, img_id)
    coefs_b = np.array(coefs_b)
    inter_row_pxl = (coefs_b[1:] - coefs_b[:-1]).mean()
    return inter_row_cm / inter_row_pxl


def calculate_dists_to_row(rows, coco_data, img_id):
    """ Calculate for each predicted plant identified with its "id" the row it belongs to

    :param rows: dict: data read in the "detected_rows.json" file
    :param coco_data: COCO: contains the predictions of the counting-av-rgbi-dl module
    :param img_id: int: identification of the image analysed
    :return: list of ndarrays: contains the distance of the sunflower centroid with each row
    """
    a_coef, b_coefs = extract_coefs_from_img_id(rows, img_id)

    # build result list
    list_dist_to_rows = []

    centroids = extract_feature_from_anns(coco_data, "centroid", img_id)
    anns_ids = extract_feature_from_anns(coco_data, "id", img_id)

    for anns_id, centroid in zip(anns_ids, centroids):
        dists_to_row = []
        for b_coef in b_coefs:
            dists_to_row.append(np.abs((a_coef * centroid[0] + b_coef) - centroid[1]))

        list_dist_to_rows.append({"annotation_id": anns_id,
                                  "distances": np.array(dists_to_row)})

    return list_dist_to_rows


def binarize_image(image):
    """ Create a new image where the pixels out of the plot have the the value 0.0 and the pixels in the plot, 1.0

    :param image: ndarray: image of the microplot
    :return: ndarray: created image
    """
    binary_image = np.zeros((image.shape[0], image.shape[1])).astype(np.float64)
    binary_image[np.where(image[:, :, :].sum(axis=2) > 0)] = 1.0
    return binary_image


def remove_outliers(areas):
    """ Remove the outliers with a difference of +/- 3 * standard deviations with the mean

    :param areas: list: contains the surface of each sunflower detected
    :return: kept_areas: list of float: contains the areas good for analysis
    """
    standard_deviation = areas.std()
    mean = areas.mean()

    kept_areas = []
    for area in areas:
        if (area < mean + 3 * standard_deviation or
                area > mean + 3 * standard_deviation):
            kept_areas.append(area)

    return kept_areas


