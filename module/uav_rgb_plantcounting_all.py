"""
Entry point script for My Module.

TODO: Adds description about My Module
"""

import json
from pathlib import Path
import logging

import numpy as np
from pycocotools.coco import COCO
import cv2
import matplotlib.pyplot as plt
import scipy.signal as sgl

from module import utils


logger = logging.getLogger('__main__')


def save_coco_json(coco_json, path):
    """ Save the dataset

    :param coco_json: dict: data produced by the counting-av-rgbi-dl module
    :param path: path to the directory where to save the json file
    """
    out_file_json = open(path + "\\corrected_coco_json.json", "w")

    json.dump(coco_json.dataset, out_file_json, indent=4)
    out_file_json.close()


def write_results(agro_var, out_path):
    out_file = open(out_path + "\\agronomics_variables.json", "w")
    json.dump(agro_var, out_file, indent=4)
    out_file.close()


def clear(coco_pth, row_pth, sf_inter_row_cm, dist_to_rm_cm, output_folder):
    """
    remove the annotations predicted in the inter-rows

    :param coco_pth: str: the path of the cocojson file resulting from the counting-av-rgbi-dl module
    :param row_pth: str: path to the json file containing the positions of the rows in the plot
    :param sf_inter_row_cm: float: distance in centimeters between two rows
    :param dist_to_rm_cm: threshold distance between the centroid of the predicted annotation and the row to remove
    the annotation
    """

    # creating new coco json variable
    with open(row_pth) as f:
        rows = json.load(f)
    coco_data = COCO(coco_pth)
    agronomic_variables = {"images": coco_data.dataset["images"],
                           "counting": []}

    image_ids = coco_data.getImgIds()
    for img_id in image_ids:
        if utils.check_rows(rows, img_id):
            dist_to_rm_pxl = dist_to_rm_cm / utils.calculate_ratio_cm_pixel(rows, sf_inter_row_cm, img_id)
            list_dists_to_rows = utils.calculate_dists_to_row(rows, coco_data, img_id)
            list_anns_ids_to_remove = []
            for dists_to_rows in list_dists_to_rows:
                if np.min(dists_to_rows["distances"]) > dist_to_rm_pxl:
                    list_anns_ids_to_remove.append(dists_to_rows["annotation_id"])
            anns_to_rm = coco_data.loadAnns(ids=list_anns_ids_to_remove)
            for ann in anns_to_rm:
                del coco_data.anns[ann["id"]]
                coco_data.dataset["annotations"].remove(ann)

            counting_var = {"image_id": img_id,
                            "number_of_sunflowers": len(coco_data.getAnnIds(imgIds=img_id))}
            agronomic_variables["counting"].append(counting_var)
        else:
            logging.debug("image id: " + str(img_id))
            coco_data.dataset["images"].remove(coco_data.imgs[img_id])
            del coco_data.imgs[img_id]

            anns_to_rm = coco_data.loadAnns(ids=coco_data.getAnnIds(imgIds=img_id))
            for ann in anns_to_rm:
                del coco_data.anns[ann["id"]]
                coco_data.dataset["annotations"].remove(ann)

    save_coco_json(coco_data, str(output_folder))
    return agronomic_variables


def assign_rows_to_anns_ids(rows, coco_data, img_id):
    """ Calculate for each predicted plant identified with its "id" the row it belongs to

    :param rows: dict: data read in the "detected_rows.json" file
    :param coco_data: COCO: contains the predictions of the counting-av-rgbi-dl module
    :param img_id: int: identification of the image analysed
    :return: ndarray: array with two columns, the first for the predicted plant id, the second for the row it belongs to
    """
    a_coef, b_coefs = extract_coefs_from_img_id(rows, img_id)

    # build result list
    rows_from_ids = []

    centroids = utils.extract_feature_from_anns(coco_data, "centroid", img_id)
    anns_ids = utils.extract_feature_from_anns(coco_data, "id", img_id)

    for anns_id, centroid in zip(anns_ids, centroids):
        dists_to_row = []
        for b_coef in b_coefs:
            dists_to_row.append(np.abs((a_coef * centroid[0] + b_coef) - centroid[1]))

        dists_to_row = np.array(dists_to_row)
        rows_from_ids.append(np.argmin(dists_to_row) + 1)

    return np.array([anns_ids, rows_from_ids])


def std(areas, rows, inter_row_cm, img_id):
    """ Calculate the standard deviation of the plant areas in cm²

    :param areas: ndarray: contains the surface of each sunflower detected
    :param rows: dict: data read in the "detected_rows.json" file
    :param inter_row_cm: float: distance in centimeters between two rows
    :param img_id: int: identification of the image analysed
    :return: the standard deviation of the given values in cm²
    """
    kept_areas_pxl = np.array(utils.remove_outliers(np.array(areas)))
    ratio = utils.calculate_ratio_cm_pixel(rows, inter_row_cm, img_id)
    areas_cm = kept_areas_pxl * (ratio ** 2)
    return areas_cm.std(), areas_cm.mean()


def calculate_mean_area_per_rank(rows, coco_data, img_id, inter_row_cm):
    """ Calculate for each row detected the mean area of the plants in it

    :param rows: dict: data read in the "detected_rows.json" file
    :param coco_data: COCO: contains the predictions of the counting-av-rgbi-dl module
    :param img_id: int: identification of the image analysed
    :param inter_row_cm: float: distance in centimeters between two rows
    :return: mean_area_per_rank: list of floats: contains the mean plant surface of each row in cm²
    :return: nb_sunflowers_area_per_rank: list of floats: contains the mean plant surface of each row in cm²
    """
    rows_from_ids_array = assign_rows_to_anns_ids(rows, coco_data, img_id)
    areas = utils.extract_feature_from_anns(coco_data, "area", img_id, rows_from_ids_array[0, :])
    sorted_areas = []
    for i in range(np.max(rows_from_ids_array[1, :])):
        sorted_areas.append([])

    for row_from_id, area in zip(rows_from_ids_array[1, :], areas):
        sorted_areas[row_from_id - 1].append(area)

    ratio_cm_pxl = utils.calculate_ratio_cm_pixel(rows, inter_row_cm, img_id)

    mean_area_per_rank = [(sum(areas_per_row)/len(areas_per_row) * (ratio_cm_pxl**2)) for areas_per_row in sorted_areas]
    nb_sunflowers_per_rank = [len(areas_per_row) for areas_per_row in sorted_areas]

    return mean_area_per_rank, nb_sunflowers_per_rank


def calc_std_central_rows(rows, coco_data, img_id, inter_row_cm):
    """ Calculate for each row detected the mean area of the plants in it

    :param rows: dict: data read in the "detected_rows.json" file
    :param coco_data: COCO: contains the predictions of the counting-av-rgbi-dl module
    :param img_id: int: identification of the image analysed
    :param inter_row_cm: float: distance in centimeters between two rows
    :return: standard deviation on the kept sunflower areas
    :return: relative standard deviation on the kept sunflower areas
    """
    rows_from_ids_array = assign_rows_to_anns_ids(rows, coco_data, img_id)
    areas = utils.extract_feature_from_anns(coco_data, "area", img_id, rows_from_ids_array[0, :])
    sorted_areas = []
    for i in range(np.max(rows_from_ids_array[1, :])):
        sorted_areas.append([])

    for row_from_id, area in zip(rows_from_ids_array[1, :], areas):
        sorted_areas[row_from_id - 1].append(area)

    kept_areas = []
    for i in sorted_areas[1: -1]:
        kept_areas.extend(i)

    standard_deviation, kept_areas_mean = std(kept_areas, rows, inter_row_cm, img_id)
    return standard_deviation, 100 * standard_deviation / kept_areas_mean


def calculate_emergence_rate(coco_path, rows_json_path, agro_var, inter_row, area_per_rank=True, plot=False):
    """ Give the standard deviation of the plants surface on each microplot and calculate also the mean
    plant surface per row

    :param coco_path: str: path to the coco json file produce by the counting-av-rgbi-dl module
    :param rows_json_path: str: path the file "detected_row.json" produced by the rowe_detection module
    :param agro_var: dict containing all the agronomics variables extracted from the detections
    :param inter_row: float: distance in centimeters between two rows in the microplot
    :param area_per_rank: boolean: if True, the mean plant surface per row is calculated
    :param plot: if True, plot the intermediary results (obsolete)
    return agro_var: dict containing all the agronomics variables extracted from the detections plus the variables
                    relative to the sunflower emergence
    """
    with open(rows_json_path) as f:
        rows = json.load(f)
    coco_data = COCO(coco_path)
    image_ids = coco_data.getImgIds()

    agro_var.update({"emergence_rates": {"dimension": "cm2", "rates": []}})
    for image_id in image_ids:
        logging.debug("image id: " + str(image_id))
        areas = np.array(utils.extract_feature_from_anns(coco_data, "area", image_id))
        standard_deviation, areas_mean = std(areas, rows, inter_row, image_id)
        std_central_rows, rel_std_central_rows = calc_std_central_rows(rows, coco_data, image_id, inter_row)
        logging.debug("standard deviation: " + str(standard_deviation) + "cm2")
        if area_per_rank:
            mean_areas_per_rank, nb_sunflowers_per_rank  = calculate_mean_area_per_rank(rows, coco_data, image_id, inter_row)
            logging.debug("relative standard deviation: " +
                  str(100 * standard_deviation / areas_mean) + "%")
            logging.debug("Mean area per rank is: " + str(mean_areas_per_rank))
            rates = {"image_id": image_id,
                     "standard_deviation": standard_deviation,
                     "relative_standard_deviation": 100 * standard_deviation / areas_mean,
                     "standard_deviation_central_rows": std_central_rows,
                     "relative_standard_deviation_central_rows": rel_std_central_rows,
                     "nb_sunflowers_per_rank": nb_sunflowers_per_rank,
                     "nb_sunflowers_central_rows": sum(nb_sunflowers_per_rank[1: -1]),
                     "mean_area_per_rank": mean_areas_per_rank}
            agro_var['emergence_rates']['rates'].append(rates)

    return agro_var


def calculate_a_b_coefs(centroids, nb_ranks):
    """ Calculate for each pair of centroids the equation (y = ax + b) of the lines passing through those two centroids

    :param centroids: list of coordinates indicating the positions of the centroids of each sunflower polygon
    :param nb_ranks: integer representing the number or fank in one microplot
    :return: ndarray containing the a and b coefficients
    """
    centers = np.array(centroids)
    centers = centers
    if nb_ranks > 1:
        # Calculate all affine functions passing through two different points (y = ax + b)
        coefs = []
        for i in centers:
            for j in centers:
                if i[0] != j[0] and i[1] != j[1]:
                    a = (i[1] - j[1]) / (i[0] - j[0])
                    b = (i[0] * j[1] - j[0] * i[1]) / (i[0] - j[0])
                    coefs.append([a, b])

        return np.array(coefs)


def select_a_coef(coefs):
    """ Determine the slope (= a coefficient) of all the lines parallel to the rows. Remove all the other lines
    from the coefficients

    :param coefs: ndarray containing the a and b coefficients of each affine function passing through
    two sunflower centroids
    :return: ndaarray containing all the a and b coefficients of the lines parallel to the rows, and the slope
    of the rows
    """
    # Parameters
    half_width_of_the_peak = 0.70
    range_kept_coefs = 5

    ## plot
    # plt.subplot(1, 2, 1)
    # plt.scatter(coefs[:, 0], coefs[:, 1])
    # plt.subplot(1, 2, 2)

    # build histogram of a values
    histogram_a = plt.hist(coefs[:, 0], bins=1000, range=(-half_width_of_the_peak, half_width_of_the_peak))

    # plt.show()
    plt.clf()

    # select only the coefficients in the histogram_a peak
    slope_borders = [histogram_a[1][np.argmax(histogram_a[0]) - range_kept_coefs],
                     histogram_a[1][np.argmax(histogram_a[0]) + (range_kept_coefs + 1)]]
    slope = (histogram_a[1][np.argmax(histogram_a[0])] + histogram_a[1][np.argmax(histogram_a[0]) + 1]) / 2

    selected_coefs_indexes = np.where(np.logical_and(coefs[:, 0] < slope_borders[1], coefs[:, 0] > slope_borders[0]))
    return coefs[selected_coefs_indexes, 1], slope


def detect_b_coefs(coefs):
    """ Detect the rows using b coefficients and an adaptative threshold

    :param coefs: ndarray containing a and b coefficients of the lines parallel to the rows
    :return: ndarray containing the b coefficients of the rows
    """
    # Parameters
    length_zero_padding = 30
    prominence_coef_threshold = 1 / 50

    # build histogram of b values
    # plt.subplot(1, 2, 1)
    histogram_b = plt.hist(coefs[0], bins=1000)

    # filter and select peak
    sos = sgl.butter(2, 35, 'lp', fs=1000, output='sos')
    zero_padded = np.zeros(histogram_b[0].shape[0] + length_zero_padding * 2)
    zero_padded[length_zero_padding: -length_zero_padding] = histogram_b[0]
    filtered = sgl.sosfilt(sos, zero_padded)

    # plt.subplot(1, 2, 2)
    # plt.plot(filtered)
    # plt.show()
    plt.clf()

    peaks_indexes = sgl.find_peaks(filtered, prominence=np.max(filtered) * prominence_coef_threshold)[0]

    if len(peaks_indexes) > 6:
        prominences = sgl.peak_prominences(filtered, peaks_indexes)[0]
        peaks_indexes = peaks_indexes[np.argsort(prominences)[-6:]]
        peaks_indexes = peaks_indexes[np.argsort(peaks_indexes)]
    peaks_indexes = peaks_indexes - length_zero_padding

    # calculate ranks b values from peaks indexes
    return peaks_indexes * ((histogram_b[1][-1] - histogram_b[1][0]) / len(histogram_b[1])) + histogram_b[1][0]


def correct_b_coefs(b_coefs, a_coef, nb_ranks, image):
    """ Check if the number of rows detected corresponds to the real number of rows. If no, interpolate the missing rows

    :param b_coefs: ndarray cotaining the b coefficients of the rows
    :param a_coef: integer representing the slope of the rows (useful if plots are required)
    :param nb_ranks: integer representing number of rows in the microplot
    :param image: ndarray containing the RGB image of the microplot
    :return:    False if less than four rows were detected
                ndarray containing the corrected b coefficients otherwise
    """

    # check if a row of weeds is detected (too close to the others)
    gaps = b_coefs[1:] - b_coefs[:-1]
    gaps_mean = gaps.mean()
    error = False

    for gap in gaps:
        if gap < 2 * gaps_mean / 3:
            error = True

    if b_coefs.shape[0] >= nb_ranks - 2 and not error:
        if b_coefs.shape[0] < nb_ranks:
            gaps = b_coefs[1:] - b_coefs[:-1]
            gap = np.min(gaps)
            good_ranks = [b_coefs[0]]
            N = 0
            for i in gaps:
                N += 1
                if i > gap * 1.5:
                    good_ranks.append(good_ranks[-1] + gap)
                    if i > gap * 2.5:
                        good_ranks.append(good_ranks[-1] + gap)
                good_ranks.append(b_coefs[N])

            if len(good_ranks) < nb_ranks:
                if good_ranks[0] < image.shape[1] - good_ranks[-1]:
                    good_ranks.append(good_ranks[-1] + gap)
                else:
                    good_ranks.insert(0, good_ranks[0] - gap)

            if len(good_ranks) < nb_ranks:
                if good_ranks[0] < image.shape[1] - good_ranks[-1]:
                    good_ranks.append(good_ranks[-1] + gap)
                else:
                    good_ranks.insert(0, good_ranks[0] - gap)

        else:
            good_ranks = b_coefs

        # plot the ranks
        # img2 = image.copy()
        # print(good_ranks)
        # for i in good_ranks:
        #     cv2.line(img2, (0, int(i)), (int(image.shape[1]), int(i + a_coef * image.shape[1])), (0, 0, 255), 2)
        # plt.imshow(img2)
        # plt.show()

        # print(np.array(good_ranks))
        return np.array(good_ranks)

    else:
        logging.debug("la parcelle ne doit pas être comprise dans les analyses")

        return False


def detect_rows(centers, nb_ranks, img):
    """ detect the position of the rows in the microplot

    :param centers: list containing centroids of the sunflower
    :param nb_ranks: integer containing the number of rows in a microplot
    :param img: ndarray containing the RGBi data
    :return: dict containing the slope (a) and the b coefficient for each row
    """
    coefs = calculate_a_b_coefs(centers, nb_ranks)
    selected_coefs, a = select_a_coef(coefs)
    detected_b = detect_b_coefs(selected_coefs)
    b = correct_b_coefs(detected_b, a, nb_ranks, img)
    if b is False:
        return None
    else:
        return {"a": a,
                "b": b.tolist()}


def extract_coefs_from_img_id(rows, img_id):
    """ Extract the affine function coefficients corresponding to the image identification number

    :param rows: dict containing the results of the row detection module
    :param img_id: int: indentification number of the image
    :return:    coefs_a: float: slope of the rows
                coefs_b: list of floats: b coefficient of the affine function corresponding to each row
    """
    ids = np.array(rows["rows"]['images']['ids'])
    coefs = rows["rows"]["coefs"]
    img_id_coefs = coefs[np.where(ids == img_id)[0][0]]
    coefs_b = img_id_coefs["b"]
    coefs_a = img_id_coefs["a"]
    return coefs_a, coefs_b


def calculate_radius(rows, img_id, inter_row_cm, disc_radius_cm):
    """ Calculate the radius of the discs in pixel

    :param rows: dict: data read in the "detected_rows.json" file
    :param img_id: int: identification number of the image
    :param inter_row_cm: float: distance in centimeters between two rows
    :param disc_radius_cm: float: radius of the discs in centimeters
    :return: float: radius of the disc to plot in pixels
    """
    ratio_cm_pixel = utils.calculate_ratio_cm_pixel(rows, inter_row_cm, img_id)
    disc_radius_pxl = disc_radius_cm / ratio_cm_pixel
    return disc_radius_pxl


def extract_centroids(img_id, coco_data):
    """ Extract for the cocojson data the centroid of each plant

    :param img_id: integer : the id of the image (1 image for 1 microplot)
    :param coco_data: dict : the data read in the coco_json file produced by the counting-av-rgbi-dl module
    :return: ndarray containing the coordinates [x, y] of each centroids in the image (in pixels)
    """
    anns_ids = coco_data.getAnnIds(imgIds=img_id)
    anns = coco_data.loadAnns(ids=anns_ids)
    centroids = []
    for annotation in anns:
        #
        centroids.append([annotation["bbox"][0] + annotation["bbox"][2] / 2,
                          annotation["bbox"][1] + annotation["bbox"][3] / 2])

    return np.array(centroids)


def extract_image(img_id, coco_data):
    """ Read the RGB image of the plot with the id of the image and the data read in the json file produced by
    the counting-av-rgbi-dl module

    :param img_id: int: identification number of the image in the cocojson data
    :param coco_data: dict : data produced by the counting-av-rgbi-dl module
    :return: ndarray: the image corresponding the id
    """
    pth = coco_data.loadImgs(img_id)[0]["path"]
    image_bgr = cv2.imread(pth)
    image_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
    return image_rgb


def save_rows_json(rows, path):
    """ Save the coefficients of the detected rows in a json file

    :param rows: dict: coefficients of the detected rows
    :param path: path to the directory where to save the json file
    """
    out_file = open(path + "\\detected_rows.json", "w")
    json.dump({"rows": rows}, out_file, indent=4)
    out_file.close()


def detect(path, nb_ranks, output_folder):
    """ produce a json file containing the position of each row in the image, from the results of the
    counting-av-rgbi-dl module and save it

    :param path: str: the path of the cocojson file resulting from the counting-av-rgbi-dl module
    :param nb_ranks: int: the number of rows in the microplot
    """
    # start variables
    output_folder = str(output_folder)
    coco_data = COCO(path)
    image_ids = coco_data.getImgIds()
    rows = {"images": {
        "paths": [],
        "ids": []
    },
        "coefs": []}

    for img_id in image_ids:
        # processing
        centroids = utils.extract_feature_from_anns(coco_data, "centroid", img_id)
        img = extract_image(img_id, coco_data)
        coefs = detect_rows(centroids, nb_ranks, img)

        # build the json file
        rows["images"]["paths"].append(coco_data.loadImgs(img_id)[0]["path"])
        rows["images"]["ids"].append(img_id)
        rows["coefs"].append(coefs)

    logging.debug("ranks identified with success !")
    save_rows_json(rows, output_folder)


def add_disks(bin_img, centroids, radius):
    """ Create a new image where sunflower centroids are replace by discs with the value 0.5, th rest of the plot
    has the value 1.0, and the pixels out of the plot has the value 0.0

    :param bin_img: ndarray: image where the pixels out of the plot have the the value 0.0 and the pixels
                             in the plot, 1.0
    :param centroids: ndarray: positions [x, y] of the sunflower centroids in the image in pixels
    :param radius: float: radius in pixel of the discs to add
    :return: ndarray: created image with the discs
    """
    disc_img = np.ones((bin_img.shape[0], bin_img.shape[1]))

    for centroid in centroids:
        cv2.circle(disc_img, (centroid[0].astype(np.uint32), centroid[1].astype(np.uint32)), 0, 0.5,
                   thickness=int(radius * 2))
    disc_img[np.where(bin_img == 0.0)] = 0.0

    return disc_img


def calculate_ratio(processed_img):
    """ Calculate the ration between the surface covered by the discs and the surface of the microplot

    :param processed_img: ndarray: image with the discs
    :return: float: the calculated ratio
    """

    x_ground = np.where(processed_img == 1.0)[0].shape[0]
    x_sunflower = np.where(processed_img == 0.5)[0].shape[0]
    return x_sunflower / (x_ground + x_sunflower)


def reduce_plot_with_cv_line(bin_img, rows, img_id, ignore_side_rows):
    """
    Crop the plot to reduce edge effects. 30% of the inter-row between two plots are kept if the side rows are
    analysed. If the side rows are ignored, the plot is cropped in the middle of the inter-row between the last row and
    the penultimate one.

    :param: bin_img: ndarray containing the processed images with disks
    :param: rows: dictionnary containing the positions of the rows in the image
    :param: img_id: integer
    :param: ignore_side_rows: bollean indicating if the side rows are analysed or ignored
    :return: ndarray containing the processed image cropped
    """
    # PARAMETERS
    margin = 500
    inter_row_percent_saved = -50 if ignore_side_rows else 30
    x_margin = 1000

    reduced_img = bin_img
    coef_a, coefs_b = extract_coefs_from_img_id(rows, img_id)
    coefs_b = np.array(coefs_b)
    inter_row_pxl = (coefs_b[1:] - coefs_b[:-1]).mean()
    # top
    for coef_b, const in zip([coefs_b[0], coefs_b[-1]], [-1, 1]):
        start_pt = (- x_margin * coef_a) + coef_b + margin * const + inter_row_pxl * \
                   inter_row_percent_saved * 0.01 * const
        end_pt = (reduced_img.shape[1] + x_margin) * coef_a + coef_b + margin * const + inter_row_pxl * \
                 inter_row_percent_saved * 0.01 * const
        cv2.line(reduced_img, (-x_margin, int(start_pt)), (x_margin + reduced_img.shape[1], int(end_pt)),
                 thickness=int(2 * margin * np.cos(np.arctan(coef_a))), color=0.0)  # 2*margin*np.sqrt(1 + coef_a ** 2)

    # for coef_b in coefs_b:
    #     start_pt = coef_b
    #     end_pt = reduced_img.shape[1] * coef_a + coef_b
    #     cv2.line(reduced_img, (0, int(start_pt)), (reduced_img.shape[1], int(end_pt)), thickness=3, color=0.75)

    # plt.imshow(reduced_img)
    # plt.show()

    return reduced_img


# def reduce_plot(bin_img, rows, img_id):
#     # PARAMETERS
#     inter_row_percent_saved = 20
#
#     reduced_img = bin_img
#     coef_a, coefs_b = extract_coefs_from_img_id(rows, img_id)
#     coefs_b = np.array(coefs_b)
#     inter_row_pxl = (coefs_b[1:] - coefs_b[:-1]).mean()
#     # le haut
#     for y, column in enumerate(bin_img):
#         for x, pix in enumerate(column):
#             if y < (coef_a * x + coefs_b[0] - 0.01 * inter_row_percent_saved * inter_row_pxl) or \
#                     y > (coef_a * x + coefs_b[-1] + 0.01 * inter_row_percent_saved * inter_row_pxl):
#                 reduced_img[y, x] = 0.0
#
#     # plt.imshow(reduced_img)
#     # plt.show()
#
#     return reduced_img


def save_stoking_density_results(rate, json_path):
    with open(json_path) as f:
        agro_var = json.load(f)

    agro_var.update({"stocking_density": rate})
    out_file = open(json_path, "w")
    json.dump(agro_var, out_file, indent=4)
    out_file.close()


def calculate_stocking_density(coco_json_path, agro_var, rows_json_path, inter_row, disc_radius,
                               ignore_side_rows):
    """ Produce an indicator on the stocking density and its heterogeneity. Each centroid is represented by
    a disc which represents the surface that the sunflower may cover when it grows up. The indicator is the ratio
    between the surface covered by the discs and the whole microplot surface.

    :param coco_json_path: str: path to the cocojson file produced by the counting-av-rgbi-dl module
    :param agro_var: dict: contains agronomics variables already calculated
    :param rows_json_path: str: path to the json file containing the positions of the rows in the plot
    :param inter_row: float: distance in centimeters between two rows
    :param disc_radius: float: radius of the discs in centimeters
    :param ignore_side_rows: boolean: indicate if the side rows should be ignore in the analysis
    """
    with open(rows_json_path) as f:
        rows = json.load(f)
    coco_data = COCO(coco_json_path)
    image_ids = coco_data.getImgIds()

    agro_var.update({"stocking_density": []})

    for img_id in image_ids:
        centroids = np.array(utils.extract_feature_from_anns(coco_data, "centroid", img_id))
        img = extract_image(img_id, coco_data)

        bin_img = utils.binarize_image(img)

        radius = calculate_radius(rows, img_id, inter_row, disc_radius)

        processed_img = add_disks(bin_img, centroids, radius)

        processed_img = reduce_plot_with_cv_line(processed_img, rows, img_id, False)
        processed_img_ignored = reduce_plot_with_cv_line(processed_img.copy(), rows, img_id, True)

        rates = []
        # plt.subplot(2, 1, 1)
        # plt.imshow(processed_img)
        # plt.subplot(2, 1, 2)
        # plt.imshow(processed_img_ignored)
        # plt.show()

        rates.append(calculate_ratio(processed_img))
        rates.append(calculate_ratio(processed_img_ignored))

        logging.debug("stocking dentsity (full rows): " + str(rates[0] * 100) + "%")
        logging.debug("stocking dentsity (side rows ignored): " + str(rates[1] * 100) + "%")
        stock_dens = {"image_id": img_id,
                      "density_full_rows": rates[0],
                      "density_side_rows_ignored": rates[1]}
        agro_var["stocking_density"].append(stock_dens)

    return agro_var


def run(output_folder, coco_path, number_rows, inter_row, calc_emergence_rate, calc_stocking_density,
        ignore_side_rows, disc_radius, dist_to_remove):
    if disc_radius is None:
        disc_radius = 40
    if dist_to_remove is None:
        dist_to_remove = 15

    detect(str(coco_path), number_rows, output_folder)
    logging.debug("detecting rows...")
    rows_path = str(output_folder) + "\\detected_rows.json"
    logging.debug("rows detected !!")

    logging.debug("removing detections in the inter-row...")
    agro_var = clear(str(coco_path), rows_path, inter_row, dist_to_remove, output_folder)
    logging.debug("done !")

    # Path to json files created in clear
    corrected_coco_path = str(output_folder) + "\\corrected_coco_json.json"
    if calc_stocking_density:
        logging.debug("\nYou choose to calculate the the stocking density ! Go !")
        agro_var = calculate_stocking_density(corrected_coco_path, agro_var, rows_path, inter_row, disc_radius,
                                              ignore_side_rows)
        logging.debug("Done !")

    if calc_emergence_rate:
        logging.debug("\nYou choose to calculate the agronomics variables relative to the emergence of the plants ! Go !")
        agro_var = calculate_emergence_rate(corrected_coco_path, rows_path, agro_var, inter_row, area_per_rank=True)
        logging.debug("Done !!")

    write_results(agro_var, str(output_folder))
