import unittest
from pathlib import Path


from module import uav_rgb_plantcounting_all


class TestCore(unittest.TestCase):
    """
    Test case for core methods

    TODO: Add other test cases. For help refer to this tutorial: https://docs.python.org/3/library/unittest.html
    """

    def test_run(self):
        uav_rgb_plantcounting_all.run(
            output_folder=Path("data\\results"),
            coco_path=Path("data\\23-06-08_11-08-06_coco.json"),
            number_rows=6,
            inter_row=50,
            calc_emergence_rate=True,
            calc_stocking_density=True,
            ignore_side_rows=False,
            disc_radius=None,
            dist_to_remove=None
        )


if __name__ == '__main__':
    unittest.main()
