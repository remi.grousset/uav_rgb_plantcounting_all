# 4P Module Model Python
**Model module in Python for 4P integration**
Follow this structure for any module that should be integrated in 4P:


## Mandatory for prototypes

***Folder "module"*** should contain the code (sources) to run the module, this includes
 - core.py (rename with your module nale, e.g. for exmaple: rgb_extract.py)
 - error.py (part of the code to mange errors in the module, like exception raised when inputs are empty)
 - utils.py (useful functions for your code that you may reuse at the several places but no necessarily the core algorithm)
 
Each function must be commented with at least the list, meaning and expected values   of inputs/outputs (type, range, eventually default values) and a short description of what the module is intended to do. 
Please also comment each step of the module so that someone who knows the principles of the algorithm can understand how it is coded.

***File "4P_Template_Documentation_Module.docx"***
Describes the module inputs/outputs and the algorithm principles. This is the main documentation of the module and will be made availabel on 4P. This document must be updated with each one of your commit.

## Mandatory for integrating the module into 4P

***File "Dockerfile"***
Dockerfile corresponding to the dockerized module

***File "inra4p.build.groovy"***
Allows to ensure the integration of the docker into 4P
see for more information https://4p.pages.mia.inra.fr/documentation/user-guide/modules/add-module.html

***File "requirements.txt"***


## Optional but recommended

***Folder "data"*** should contain test data to test that the module is well integrated in 4P. Make sure that this folder is not too large.

***Folder "test"*** should contain unitary test functions


